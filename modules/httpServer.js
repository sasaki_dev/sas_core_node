//region node.js core

//endregion
//region npm modules

//endregion
//region modules

//endregion

/**
 @class HttpServer
 */
HttpServer = function () {
    var _self = this;

    //region private fields and methods
    var _appsBySubdomain = {};
    var _init = function () {

    };

    var _onRequest = function(req, res) {

    };

    //endregion

    //region public API
    this.addSubdomain = function (subdomain, app) {
        _appsBySubdomain[subdomain] = app;
    };


    //endregion

    _init();
};

module.exports = HttpServer;

