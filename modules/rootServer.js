//region node.js core

//endregion
//region npm modules

//endregion
//region modules

//endregion

/**
 @class RootServer
 */
RootServer = function () {
    var _self = this;

    //region private fields and methods
    var _httpServer;

    var _init = function () {
        _httpServer = new (require('./httpServer'))();

        require('http').globalAgent.maxSockets = 100000;//allow plenty of connections for long-running CartoDB tiles
        require('https').globalAgent.maxSockets = 1000;//typically not as many https connections...

        process.on('uncaughtException', function (err) {
            console.log('WARNING: uncaught exception: ' + err);
        });
    };

    //endregion

    //region public API
    this.addApp = function (subdomain, app) {
        _httpServer.addSubdomain(subdomain, app);
    };

    //endregion

    _init();
};

module.exports = RootServer;

