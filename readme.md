Core modules for Node.js

## Read-only installation

Include this file using NPM:

```
npm install git+https://USERNAME@bitbucket.org/sasaki_dev/sas_core_node.git
```

## Installation for WebStorm Development

- use git to check out solution into npm node_modules folder (can be anywhere above the project folder, Node.js will find it)

- add the sas_core_node folder to WebStorm using Settings > Directories > Add Content Root.